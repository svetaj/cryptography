import sys

def strxor(a, b):     # xor two strings of different lengths
    if len(a) > len(b):
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a[:len(b)], b)])
    else:
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b[:len(a)])])

def encrypt(key, msg):
    c = strxor(key, msg)
    print (c.encode('hex'))
    return c

m1="attack at dawn"
c1="09e1c5f70a65ac519458e7e53f36".decode('hex')
m2="attack at dusk"

k=strxor(m1,c1)
print(strxor(m2,k).encode(hex))
