import sys

def strxor(a, b):     # xor two strings of different lengths
    if len(a) > len(b):
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a[:len(b)], b)])
    else:
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b[:len(a)])])

iv0x='20814804c1767293b99f1d9cab3bc3e7' 
c00x='ac1e37bfb15599e5f40eef805488281d'

iv0=iv0x.decode('hex')
c00=c00x.decode('hex')

m00='Pay Bob 100$'
m01='Pay Bob 500$'
m00x=m00.encode('hex')
m01x=m01.encode('hex')
padx='04040404'
pad=padx.decode('hex')
m00px=m00x+padx
m01px=m01x+padx
m00p=m00px.decode('hex')
m01p=m01px.decode('hex')

#print(iv0x)
#print(c00x)
#print(m00x)
#print(m01x)
x1=strxor(m00,m01)
x2=strxor(m00p,m01p)
#print(x1.encode('hex'))
#print(x2.encode('hex'))
x3=strxor(x2,iv0)
print(x3.encode('hex'))
print(iv0x)
