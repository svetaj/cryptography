import sys


def strxor(a, b):     # xor two strings of different lengths
    if len(a) > len(b):
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a[:len(b)], b)])
    else:
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b[:len(a)])])

k1='In this letter I make some remarks on a general principle relevant to enciphering in general and my machine.'.encode('hex')
k2='The significance of this general conjecture, assuming its truth, is easy to see. It means that it may be feasible to design ciphers that are effectively unbreakable.'.encode('hex')
k3='If qualified opinions incline to believe in the exponential conjecture, then I think we cannot afford not to make use of it.'.encode('hex')
k4='We see immediately that one needs little information to begin to break down the process.'.encode('hex')
print (k1)
print (k2)
print (k3)
print (k4)
