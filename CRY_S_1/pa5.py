import gmpy2
from gmpy2 import mpz

# 
# h = g^x in Zp   x=?
# B=2**20
# h*(g^-x1) mod p    x1[0..B]   -----> hash     
# (g^B)^x0  mod p    x0[0..B]  <-----> hash
# x = x0*B +x1 mod p
p=mpz(13407807929942597099574024998205846127479365820592393377723561443721764030073546976801874298166903427690031858186486050853753882811946569946433649006084171)
g=mpz(11717829880366207009516117596335367088558084999998952205599979459063929499736583746670572176471460312928594829675428279466566527115212748467589894601965568)
h=mpz(3239475104050450443565264378728065788649097520952449527834792452971981976143292558073856937958553180532878928001494706097394108577585732452307673444020333)

print p
print g
print h
print '=================================================================='

B = 2**20
print 'B='
print B
ht = {}

i = 0;
while True:
    if i > B: break
    x1=i
    i=i+1
    t1=gmpy2.powmod(g,x1,p)
    t2=gmpy2.invert(t1,p)
    t3=gmpy2.mul(h,t2)
    t4=gmpy2.f_mod(t3,p)
    ht[str(t4)]=x1

print 'end of loop 1, i='
print i

j = 0;
solution = False
while True:
    if j > B: break
    x0=j
    j=j+1
    t1=gmpy2.powmod(g,B,p)
    t2=gmpy2.powmod(t1,x0,p)
    inhash = True
    try:
        x1 = ht[str(t2)]
    except KeyError:
        inhash = False
        pass
    if inhash == True:
        solution = True
        result = gmpy2.mul(x0,B)+x1
        break

print 'Solution='
print solution
print 'end of loop 2, j='
print j

print '=================================================='
print 'result is'
print result
print '=================================================='
print 'check'
print gmpy2.powmod(g,result,p)
print 'should be'
print h
