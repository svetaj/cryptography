import sys


def strxor(a, b):     # xor two strings of different lengths
    if len(a) > len(b):
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a[:len(b)], b)])
    else:
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b[:len(a)])])

x11="9f970f4e932330e4".decode('hex')
x12="6068f0b1b645c008".decode('hex')
x21="7c2822ebfdc48bfb".decode('hex')
x22="325032a9c5e2364b".decode('hex')
x31="7b50baab07640c3d".decode('hex')
x32="ac343a22cea46d60".decode('hex')
x41="5f67abaf5210722b".decode('hex')
x42="bbe033c00bc9330e".decode('hex')
print (strxor(x11,x12).encode('hex'))
print (strxor(x21,x22).encode('hex'))
print (strxor(x31,x32).encode('hex'))
print (strxor(x41,x42).encode('hex'))
