import urllib2
import sys

def strxor(a, b):     # xor two strings of different lengths
    if len(a) > len(b):
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a[:len(b)], b)])
    else:
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b[:len(a)])])
    
TARGET = 'http://crypto-class.appspot.com/po?er='

#--------------------------------------------------------------
# padding oracle
#--------------------------------------------------------------
class PaddingOracle(object):
    def query(self, q):
        res = 'E'
        target = TARGET + urllib2.quote(q)    # Create query URL
        req = urllib2.Request(target)         # Send HTTP request to server
        try:
            f = urllib2.urlopen(req)          # Wait for response
        except urllib2.HTTPError, e:          
            print ("[%d]" % e.code),       # Print response code            
            if e.code == 403:
                return 'A' # bad pad
            if e.code == 404:
                return 'B' # OK pad case 1
            if e.code == 200:
                return 'C' # OK pad case 2
        else:
            print ("[OK!]"),       # Print response code            
            return 'D' # everything else

po = PaddingOracle()

#    12345678901234567890123456789012
c0x='f20bdba6ff29eed7b046d1df9fb70000'
c1x='58b1ffb4210a580f748b4ac714c001bd'
c2x='4a61044426fb515dad3f21f18aa577c0'
c3x='bdf302936266926ff37dbf7035d5eeb4'
cx= [c0x,c1x,c2x,c3x]

p0 ='00000000000000000000000000000000'
p1 ='00000000000000000000000000000001'
p2 ='00000000000000000000000000000202'
p3 ='00000000000000000000000000030303'
p4 ='00000000000000000000000004040404'
p5 ='00000000000000000000000505050505'
p6 ='00000000000000000000060606060606'
p7 ='00000000000000000007070707070707'
p8 ='00000000000000000808080808080808'
p9 ='00000000000000090909090909090909'
p10='0000000000000a0a0a0a0a0a0a0a0a0a'
p11='00000000000b0b0b0b0b0b0b0b0b0b0b'
p12='000000000c0c0c0c0c0c0c0c0c0c0c0c'
p13='0000000d0d0d0d0d0d0d0d0d0d0d0d0d'
p14='00000e0e0e0e0e0e0e0e0e0e0e0e0e0e'
p15='000f0f0f0f0f0f0f0f0f0f0f0f0f0f0f'
p16='10101010101010101010101010101010' 
p=[p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16]

for mm in range (2,-1,-1):
    guess=''
    k=-1
    for m in range(30,-2,-2):
        print
        print (p0[0:m]+'@@'+guess)
        print (cx[mm])
        print ('========= guess='+guess+'=')
        res='D'
        k=k+1
        r=1
        if (mm < 2): r=20
        for i in range(r,256) :
                 ii=hex(i)[2:]
                 if i < 16: ii='0'+ii
                 print (ii),
                 yy=p[k][30:]
                 trt=p0[0:m]+ii+guess
                 cxx=strxor(strxor(cx[mm].decode('hex'),trt.decode('hex')),p[k].decode('hex')).encode('hex')
                 if (mm == 0): qq=cxx+cx[1]
                 if (mm == 1): qq=cx[0]+cxx+cx[2]
                 if (mm == 2): qq=cx[0]+cx[1]+cxx+cx[3]
                 res = po.query(qq)       # Issue HTTP query with the given argument
                 if (res == 'B' or (m < 30 and res == 'D')):
                     guess = ii+guess
                     break

    print                
    print '======================================================================== guess='+guess.decode('hex')+'='
