#!/bin/sh

./xscript1.py > xtext
./xscript2.py > xascii

> xtemp
for x in {1..83}
do
    CODES=`cut -d" " -f$x xtext`
    > xdecode
    for y in $CODES
    do
        grep "^$y " xascii >> xdecode
    done
    cut -d" " -f2 xdecode | sort > xdecode1
    for x in `sort -u xdecode1`; do grep -c $x xdecode1; echo $x; done | paste  - - | sort -nr |  head -1 >> xtemp
done
cat xtemp | tr  '\t' ' ' | cut -d" " -f2 | tr '@' ' ' | tr -d '\n'
echo ''

rm xascii xdecode xdecode1 xtemp xtext
