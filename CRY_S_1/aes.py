#!/usr/bin/python

import sys

from Crypto.Cipher import AES
from Crypto.Util import Counter

cbckey1='140b41b22a29beb4061bda66b6747e14'.decode('hex')
cbccthex1='4ca00ff4c898d61e1edbf1800618fb2828a226d160dad07883d04e008a7897ee2e4b7465d5290d0c0e6c6822236e1daafb94ffe0c5da05d9476be028ad7c1d81'

iv1=cbccthex1[:32].decode('hex')
cbcct1=cbccthex1[32:].decode('hex')
cipher1 = AES.new(cbckey1, AES.MODE_CBC, iv1)
msg1=cipher1.decrypt(cbcct1)
print(msg1.encode('hex'))
print("-->"+msg1+"<--")

cbckey2='140b41b22a29beb4061bda66b6747e14'.decode('hex')
cbccthex2='5b68629feb8606f9a6667670b75b38a5b4832d0f26e1ab7da33249de7d4afc48e713ac646ace36e872ad5fb8a512428a6e21364b0c374df45503473c5242a253'

iv2=cbccthex2[:32].decode('hex')
cbcct2=cbccthex2[32:].decode('hex')
cipher2 = AES.new(cbckey2, AES.MODE_CBC, iv2)
msg2=cipher2.decrypt(cbcct2)
print(msg2.encode('hex'))
print("-->"+msg2+"<--")

ctrkey3='36f18357be4dbd77f050515c73fcf9f2'.decode('hex')
ctrcthex3='69dda8455c7dd4254bf353b773304eec0ec7702330098ce7f7520d1cbbb20fc388d1b0adb5054dbd7370849dbf0b88d393f252e764f1f5f7ad97ef79d59ce29f5f51eeca32eabedd9afa9329'

iv3=ctrcthex3[:32].decode('hex')
ctr = Counter.new(128, initial_value=long(iv3.encode("hex"), 16))
cipher3 = AES.new(ctrkey3, AES.MODE_CTR, counter=ctr)
ctrct3=ctrcthex3[32:64].decode('hex')
msg31=cipher3.decrypt(ctrct3)
ctrct3=ctrcthex3[64:96].decode('hex')
msg32=cipher3.decrypt(ctrct3)
ctrct3=ctrcthex3[96:128].decode('hex')
msg33=cipher3.decrypt(ctrct3)
temp=ctrcthex3[128:]+"00000000000000000000000000000000000000000000000000000"
ctrct3=temp[:32].decode('hex')
msg34=cipher3.decrypt(ctrct3)
print("-->"+msg31+msg32+msg33+msg34+"<--")

ctrkey4='36f18357be4dbd77f050515c73fcf9f2'.decode('hex')
ctrcthex4='770b80259ec33beb2561358a9f2dc617e46218c0a53cbeca695ae45faa8952aa0e311bde9d4e01726d3184c34451'

iv4=ctrcthex4[:32].decode('hex')
ctr = Counter.new(128, initial_value=long(iv4.encode("hex"), 16))
cipher4 = AES.new(ctrkey4, AES.MODE_CTR, counter=ctr)
ctrct4=ctrcthex4[32:64].decode('hex')
msg41=cipher4.decrypt(ctrct4)
temp=ctrcthex4[64:]+"00000000000000000000000000000000000000000000000000000"
ctrct4=temp[:32].decode('hex')
msg42=cipher4.decrypt(ctrct4)
print("-->"+msg41+msg42+"<--")
