#!/usr/bin/python

import sys
import os

from Crypto.Hash import SHA256

f = open('testfile1.mp4', "rb")
data = f.read()
f.close()

start = (len(data) / 1024) * 1024
tag = ''
total = 0

while (start >= 0):
   h = SHA256.new()
   block = data[start:start+1024]
   total += len(block)
   h.update(block+tag)
   tag = h.digest()
   start = start - 1024

print (tag.encode('hex'))
print (total)
print (len(data))
