import math
import gmpy2
from gmpy2 import mpz
from gmpy2 import mpfr


print '####################################################################'
print '                 Problem #3'
print '####################################################################'

N1 = '72006226374735042527956443552558373833808445147399984182665305798191'
N2 = '63556901883377904234086641876639384851752649940178970835240791356868'
N3 = '77441155132015188279331812309091996246361896836573643119174094961348'
N4 = '52463970788523879939683923036467667022162701835329944324119217381272'
N5 = '9276147530748597302192751375739387929'

NN=N1+N2+N3+N4+N5
gmpy2.get_context().precision=3000

N = mpz(NN)
FN6 = mpfr(N*6)
FN = mpfr(N)
print '-------------N----------------------------'
print N
print '-------------bit_length(N)----------------------------'
print gmpy2.bit_length(N)

A = gmpy2.sqrt(FN6)
Aint = mpz(gmpy2.ceil(gmpy2.sqrt(FN6)))
AA = mpfr(mpfr(Aint) - mpfr(0.5))
x = gmpy2.sqrt(AA*AA-FN6)
xint = mpz(gmpy2.ceil(x))
xx = mpfr(mpfr(xint) - mpfr(0.5))
pp = mpz(gmpy2.ceil(AA+xx))
qq = mpz(gmpy2.ceil(AA-xx))
p = pp/2
q = qq/3
AAA =  mpfr(2*p+3*q)/mpfr(2)

print '-------------AA----------------------------'
print AA
print '-------------xx----------------------------'
print xx
print '-------------xint----------------------------'
print xint
print '-------------p----------------------------'
print p
print '-------------q----------------------------'
print q
print '-------------AAA----------------------------'
print AAA
print '-------------p*q----------------------------'
print p*q
print '-------------N-pq----------------------------'
print N-p*q

