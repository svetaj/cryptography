import sys


def strxor(a, b):     # xor two strings of different lengths
    if len(a) > len(b):
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a[:len(b)], b)])
    else:
        return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b[:len(a)])])

x11="4af532671351e2e1" 
x12="87a40cfa8dd39154"
x21="5f67abaf5210722b" 
x22="bbe033c00bc9330e"
x31="9f970f4e932330e4" 
x32="6068f0b1b645c008"
x41="2d1cfa42c0b1d266" 
x42="eea6e3ddb2146dd0"

print (strxor(x11,x12).encode('hex'))
print (strxor(x21,x22).encode('hex'))
print (strxor(x31,x32).encode('hex'))
print (strxor(x41,x42).encode('hex'))
